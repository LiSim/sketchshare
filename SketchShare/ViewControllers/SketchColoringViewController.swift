//
//  ColoringViewController.swift
//  SketchShare
//
//  Created by AppDev on 03/04/2016.
//  Copyright © 2016 AppDev. All rights reserved.
//

import UIKit

class SketchColoringViewController: UIViewController {
    
    var sketch: Sketch!
    private var displayStrokeOptionBar: Bool!
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var strokeWidthSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.backButton.layer.cornerRadius = self.backButton.frame.size.width / 2
        self.strokeWidthSlider.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
        self.displayStrokeOptionBar = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonSelected(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
