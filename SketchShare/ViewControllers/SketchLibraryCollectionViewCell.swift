//
//  SketchLibraryCollectionViewCell.swift
//  SketchShare
//
//  Created by AppDev on 03/04/2016.
//  Copyright © 2016 AppDev. All rights reserved.
//

import UIKit

class SketchLibraryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var sketchImageView: UIImageView!
    @IBOutlet weak var sketchOptionsView: UIView!
    @IBOutlet weak var sketchOptionsViewHeightConstraint: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.sketchOptionsViewHeightConstraint.constant = 0
        self.layoutIfNeeded()
    }
    
    func showSketchOptionsBarView() {
        
        self.sketchOptionsViewHeightConstraint.constant = self.frame.size.height / 2
        UIView.animateWithDuration(0.5, animations: { 
            self.layoutIfNeeded()
        })
    }
    
    func hideSketchOptionsBarView() {
        
        self.sketchOptionsViewHeightConstraint.constant = 0
        UIView.animateWithDuration(0.5, animations: { 
            self.layoutIfNeeded()
        })
    }
}
