//
//  SketchShowcaseViewController.swift
//  SketchShare
//
//  Created by AppDev on 03/04/2016.
//  Copyright © 2016 AppDev. All rights reserved.
//

import UIKit

class SketchShowcaseViewController: UIViewController {

    var sketch: Sketch!
    var sketches: [Sketch] = []
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SketchShowcaseViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1 //self.sketches.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: SketchColorPreviewCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("SketchColorPreviewCollectionViewCell", forIndexPath: indexPath) as! SketchColorPreviewCollectionViewCell
        return cell
    }
    
    // MARK: Collection View Layout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let cellWidth: CGFloat = (self.view.frame.size.width - 20) / 2
        return CGSizeMake(cellWidth, cellWidth)
    }
}
