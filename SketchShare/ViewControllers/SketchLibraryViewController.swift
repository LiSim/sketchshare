//
//  SketchesLibraryViewController.swift
//  SketchShare
//
//  Created by AppDev on 03/04/2016.
//  Copyright © 2016 AppDev. All rights reserved.
//

import UIKit

class SketchLibraryViewController: UIViewController {
    
    enum Options: Int {
        case Coloring = 0
        case Video
        case Showcase
        
        static let count: Int = 3
    }
    var sketches: [Sketch] = []
    private var selectedCellIndexPath: NSIndexPath?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set refresh control
        let refreshControl: UIRefreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadSketches(_:)), forControlEvents: .ValueChanged)
        self.refreshControl = refreshControl
        self.collectionView.addSubview(self.refreshControl)
        
        self.loadSketches(nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // MARK: - Load sketches from server
    func loadSketches(sender: AnyObject?) {
        // self.sketches = sketches
        (sender as? UIRefreshControl)?.endRefreshing()
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showSketchColoringViewController" {
            let colorVC: SketchColoringViewController = segue.destinationViewController as! SketchColoringViewController
            colorVC.sketch = Sketch()
        } else if segue.identifier == "showSketchVideoViewController" {
            let videoVC: SketchPlaybackViewController = segue.destinationViewController as! SketchPlaybackViewController
            videoVC.sketch = Sketch()
        } else if segue.identifier == "showSketchShowcaseViewController" {
            let showcaseVC: SketchShowcaseViewController = segue.destinationViewController as! SketchShowcaseViewController
            showcaseVC.sketch = Sketch()
        }
    }
    
}

// MARK: - CollectionViewDataSource, CollectionViewDelegate
extension SketchLibraryViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1 //self.sketches.count ?? 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell: SketchLibraryCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("SketchLibraryCollectionViewCell", forIndexPath: indexPath) as! SketchLibraryCollectionViewCell
        return cell
    }
    
    // MARK: - Selected cell
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if self.selectedCellIndexPath == nil {
            
            let currentSelectedCell: SketchLibraryCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! SketchLibraryCollectionViewCell
            currentSelectedCell.showSketchOptionsBarView()
            self.selectedCellIndexPath = indexPath
            
        } else if self.selectedCellIndexPath != indexPath {
            
            let currentSelectedCell: SketchLibraryCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! SketchLibraryCollectionViewCell
            currentSelectedCell.showSketchOptionsBarView()
            let selectedCell: SketchLibraryCollectionViewCell = collectionView.cellForItemAtIndexPath(self.selectedCellIndexPath!) as! SketchLibraryCollectionViewCell
            selectedCell.hideSketchOptionsBarView()
            self.selectedCellIndexPath = indexPath
            
        } else {
            let selectedCell: SketchLibraryCollectionViewCell = collectionView.cellForItemAtIndexPath(indexPath) as! SketchLibraryCollectionViewCell
            selectedCell.hideSketchOptionsBarView()
            self.selectedCellIndexPath = nil
        }
    }
    
    // MARK: Collection View Layout
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let cellWidth: CGFloat = self.view.frame.size.width - 20
        return CGSizeMake(cellWidth, 200)
    }
    
    
}
