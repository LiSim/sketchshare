//
//  SketchColorPreviewCollectionViewCell.swift
//  SketchShare
//
//  Created by AppDev on 03/04/2016.
//  Copyright © 2016 AppDev. All rights reserved.
//

import UIKit

class SketchColorPreviewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var previewImageView: UIImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
