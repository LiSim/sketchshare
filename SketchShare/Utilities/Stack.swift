//
//  LinkedListStack.swift
//  SketchShare
//
//  Created by AppDev on 01/04/2016.
//  Copyright © 2016 Admin. All rights reserved.
//

import Foundation

// MARK : - Description
// Memory structure to push new values on top, or pop them off again to read
// Stack is created with size of <capacity>. When more elements are added to a full capacity, then Stack is increased by <increment>
// multiplier is value of H

// Integer data type to store both the x and y coordinate
// Storage format = p = h * x + y
// x = p / h
// y = p % h
let _FinalNodeOffset = -1
let _InvalidNodeContent = -2

struct Stack {
    
    var nodes: [Int] = [Int]()
    var multiplier: Int!
    
    init(multiplier: Int) {
        self.nodes.append(_FinalNodeOffset)
        self.multiplier = multiplier
    }
    
    mutating func push(x xCoordinate: Int, y yCoordinate: Int) {
        let node: Int = self.multiplier * xCoordinate + yCoordinate
        self.nodes.append(node)
    }
    
    mutating func pop() -> (x: Int, y: Int)? {
        let node: Int = self.nodes.removeLast()
        if node == _FinalNodeOffset {
            return nil
        }
        
        let xCoordinate: Int = node / self.multiplier
        let yCoordinate: Int = node % self.multiplier
        
        return (x: xCoordinate, y: yCoordinate)
    }
}